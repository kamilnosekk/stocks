import React from 'react';
import { createTheme } from '@mui/material';
import './App.css';
import Routing from './components/Routing';
import { PortfolioContextProvider } from './contexts/PortfolioContext';
import { ThemeProvider } from '@mui/styles';

const theme = createTheme({
    breakpoints: {
        values: {
            xs: 300, // phone
            sm: 600, // tablets
            md: 900, // small laptop
            lg: 1200, // desktop
            xl: 1536 // large screens
        }
    }
});

function App() {
    return (
        <ThemeProvider theme={theme}>
            <PortfolioContextProvider>
                <Routing/>
            </PortfolioContextProvider>
        </ThemeProvider>
    );
}

export default App;
