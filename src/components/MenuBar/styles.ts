import { makeStyles } from '@mui/styles';

const useMenuBarStyles = makeStyles({
    link: {
        color: 'white',
        textDecoration: 'none',
    },
});

export default useMenuBarStyles;
