import React from 'react';
import AppBar from '@mui/material/AppBar';
import Box from '@mui/material/Box';
import Toolbar from '@mui/material/Toolbar';
import Typography from '@mui/material/Typography';
import { Link } from 'react-router-dom';
import { routes } from '../../utils/consts/route-paths';
import useMenuBarStyles from './styles';

const MenuBar: React.FC = () => {
    const classes = useMenuBarStyles();

    return (
        <Box sx={{ flexGrow: 1 }}>
            <AppBar position="static">
                <Toolbar>
                    <Link className={classes.link} to={routes.DASHBOARD}>
                        <Typography variant="h6" component="div" sx={{ flexGrow: 1 }}>
                            SDH Frontend Homework
                        </Typography>
                    </Link>
                </Toolbar>
            </AppBar>
        </Box>
    );
};

export default MenuBar;
