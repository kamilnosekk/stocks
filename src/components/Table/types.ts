import React from 'react';

export interface Props<T> {
    columns: Column<T>[];
    data: T[]
    withActions: boolean;
    actions: Action<T>[];
}

export interface Action<T> {
    icon: React.ReactNode;
    handler: (data: T) => void;
}

export interface Column<T> {
    label: string;
    key: string;

    handleCellClick?: (data: T) => void;
}
