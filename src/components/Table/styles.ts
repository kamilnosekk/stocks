import { makeStyles } from '@mui/styles';

const useTableStyles = makeStyles({
    table: {
        width: '100%',
        borderCollapse: 'collapse',
        borderRadius: 5,
    },
    header: {
        border: '1px solid #dddddd',
        textAlign: 'left',
        padding: 8
    },
    cell :{
        border: '1px solid #dddddd',
        textAlign: 'left',
        padding: 8
    },
    actions: {
        cursor: 'pointer',
    },
    cellAction: {
        cursor: 'pointer',

    }
});

export default useTableStyles;
