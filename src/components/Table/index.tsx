import React, { useEffect, useState } from 'react';
import useTableStyles from './styles';
import clsx from 'clsx';
import { Column, Props } from './types';

const Table = <T extends object>({ columns, data, withActions, actions }: Props<T>) => {
    const [ cols, setCols ] = useState<Column<T>[]>([]);
    const classes = useTableStyles();

    useEffect(() => {
        withActions
            ? setCols([ ...columns, { key: 'actions', label: 'Actions' } ])
            : setCols(columns);
    }, [ columns, withActions ]);


    return (
        <table className={ classes.table }>
            <tr>
                { cols.map(({ label }) => (
                    <th key={label} className={ classes.header }>{ label }</th>
                )) }
            </tr>
            { data.map((row) => <tr>
                { cols.map(({ key, handleCellClick }) => (
                    key === 'actions'
                        ? <td className={ classes.cell }>
                            { actions.map(({ icon: Icon, handler }: any) =>
                                <Icon key={Icon} className={classes.actions} onClick={() => handler(row)}/>)}
                          </td>
                        : <td className={ clsx(classes.cell, { [classes.cellAction]: !!handleCellClick}) }
                              key={key}
                              onClick={() => handleCellClick && handleCellClick(row)}>
                            { (row as any)[key] }
                          </td>
                ))
                }
            </tr>) }
        </table>
    );
};

export default Table;
