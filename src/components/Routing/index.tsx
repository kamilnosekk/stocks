import React from 'react';
import { BrowserRouter, Navigate } from 'react-router-dom';
import { Routes, Route } from 'react-router-dom';
import { Routes as ApplicationRoutes } from 'utils/consts/routing';
import MenuBar from '../MenuBar';
import Layout from '../Layout';

const Routing = () => {
    return (
        <BrowserRouter basename={ process.env.PUBLIC_URL }>
            <Routes>
                { ApplicationRoutes.map(({ path, component: Component }) => (
                    <Route
                        path={ path }
                        key={ path }
                        element={ <Layout>
                            <MenuBar/>
                            <Component/>
                        </Layout>}
                    />
                )) }
                <Route path='*' element={ <Navigate replace to="/"/> }/>
            </Routes>
        </BrowserRouter>
    );
};

export default Routing;
