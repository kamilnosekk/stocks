import React from 'react';
import useLayoutStyles from './styles';

const Layout: React.FC = ( { children }) => {
    const classes = useLayoutStyles();

    return (
        <div className={classes.wrapper}>
            { children }
        </div>
    )
}

export default Layout;
