import { makeStyles } from '@mui/styles';

const useLayoutStyles = makeStyles({
    wrapper: {
        minWidth: '100vw',
        minHeight: '100vh',
        maxWidth: '100vw',
        overflow: 'hidden',
    },
});

export default useLayoutStyles;
