export const useLocalStorage = () => {
    const deleteFromLocalStorage = (key: string) => {
        localStorage.removeItem(key);
    }

    const getFromLocalStorage = (key: string) => {
        try {
            const storage = localStorage.getItem(key) || '';
            return JSON.parse(storage)
        } catch (e) {
            return localStorage.getItem(key)
        }
    }

    const insertToLocalStorage = (key: string, value: object) => {
        localStorage.setItem(key, JSON.stringify(value));
    }

    return { deleteFromLocalStorage, getFromLocalStorage, insertToLocalStorage };
}
