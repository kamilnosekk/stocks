import { useCallback, useState } from 'react';
import { httpReq } from '../utils/interceptors/api';

type Method = 'get' | 'post';

interface Request<T, U> {
    payload?: T;
    params?: Record<string, string>[];
    method: Method;
    callback?: (data: U) => void;
    onError?: (error: unknown) => void;
}

export const useSendData = <T, U>(
    url: string,
) => {
    const [ loading, setLoading ] = useState(false);
    const [ data, setData ] = useState<U>();
    const [ error, setError ] = useState<unknown | null>(null);

    const makeRequest = useCallback(
        async ({
                   payload,
                   method,
                   callback,
                   onError,
                   params,
               }: Request<T, U>) => {
            try {
                setLoading(true);
                setError(null);
                if(params) {
                    const allParams = params.reduce((acc, curr, index) => {
                        return index === 0
                            ? `?${acc}&${curr.param}=${curr.value}`
                            : `${acc}&${curr.param}=${curr.value}`;
                    }, '')

                    url = `${url}${allParams}`
                }

                const { data } = await httpReq[method](url, payload);

                setData(data);
                setLoading(false);

                callback && callback(data);
            } catch (err) {
                onError && onError(err);
                setLoading(false);
            } finally {
                setLoading(false);
            }
        },
        [url]
    );

    return { data, loading, error, makeRequest };
};
