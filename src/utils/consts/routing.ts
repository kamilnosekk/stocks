import { routes } from './route-paths';
import { Route } from '../models/route';
import Dashboard from '../../pages/Dashboard';
import CompanyDetails from '../../pages/CompanyDetails';

export const Routes: Route[] = [
    {
        path: routes.DASHBOARD,
        component: Dashboard,
    },
    {
        path: routes.COMPANY_DETAILS,
        component: CompanyDetails,
    },
];
