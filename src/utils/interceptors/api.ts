import axios from 'axios';

const BASIC_CONFIG = {
    url: '',
    headers: {
        'Content-Type': 'application/json'
    },
};

export const httpReq = axios.create(BASIC_CONFIG);


httpReq.interceptors.request.use((config) => {
    console.log(process.env);
    config.url = `${config.url}&apikey=${process.env.REACT_APP_API_KEY}`


    return config
})
