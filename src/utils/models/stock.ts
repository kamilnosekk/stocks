export const enum StockKeys {
    SYMBOL = '1. symbol',
    NAME = '2. name',
    TYPE = '3. type',
    REGION = '4. region',
    MARKET_OPEN = '5. marketOpen',
    MARKET_CLOSE = '6. marketClose',
    TIMEZONE = '7. timezone',
    CURRENCY = '8. currency',
    MATCH_SCORE = '9. matchScore',
}

export interface Stock {
    [StockKeys.SYMBOL]: string;
    [StockKeys.NAME]: string;
    [StockKeys.TYPE]: string;
    [StockKeys.REGION]: string;
    [StockKeys.MARKET_OPEN]: string;
    [StockKeys.MARKET_CLOSE]: string;
    [StockKeys.TIMEZONE]: string;
    [StockKeys.CURRENCY]: string;
    [StockKeys.MATCH_SCORE]: string;
}
