export interface CompanyDetails {
    Symbol: string;
    AssetType: string;
    Name: string;
    Description: string;
    CIK: string;
    Exchange: string;
    Currency: string;
    Country: string;
    Sector: string;
    Industry: string;
    Address: string;
    FiscalYearEnd: string;
    LatestQuarter: string;
    MarketCapitalization: string;
    EBITDA: string;
    PERatio: string;
    PEGRatio: string;
    BookValue: string;
    DividendPerShare: string;
    DividendYield: string;
    EPS: string;
    RevenuePerShareTTM: string;
    ProfitMargin: string;
    OperatingMarginTTM: string;
    ReturnOnAssetsTTM: string;
    ReturnOnEquityTTM: string;
    RevenueTTM: string;
    GrossProfitTTM: string;
    DilutedEPSTTM: string;
    QuarterlyEarningsGrowthYOY: string;
    QuarterlyRevenueGrowthYOY: string;
    AnalystTargetPrice: string;
    TrailingPE: string;
    ForwardPE: string;
    PriceToSalesRatioTTM: string;
    PriceToBookRatio: string;
    EVToRevenue: string;
    EVToEBITDA: string;
    Beta: string;
    [CompanyDetailsKeys.WEEK_HIGH]: string;
    [CompanyDetailsKeys.WEEK_LOW]: string;
    [CompanyDetailsKeys.DAY_MOVING_AVERAGE_50]: string;
    [CompanyDetailsKeys.DAY_MOVING_AVERAGE_200]: string;
    SharesOutstanding: string;
    DividendDate: string;
    ExDividendDate: string;
}


export const enum CompanyDetailsKeys {
    WEEK_HIGH ='52WeekHigh',
    WEEK_LOW = '52WeekLow',
    DAY_MOVING_AVERAGE_50 = '50DayMovingAverage',
    DAY_MOVING_AVERAGE_200 = '200DayMovingAverage'
}
