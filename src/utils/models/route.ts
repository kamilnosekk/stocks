export interface Route {
    path: string;
    component: () => JSX.Element;
}
