import React from 'react';
import SearchPanel from './components/SearchPanel';
import useDashboardStyles from './styles';
import SavedStocks from './components/SavedStocks';
import { Divider } from '@mui/material';

const Dashboard = () => {
    const classes = useDashboardStyles();

    return (
        <div className={classes.container}>
            <div className={classes.search}>
                <SearchPanel/>
            </div>
            <Divider className={classes.divider} orientation="vertical"/>
            <div className={classes.search}>
                <SavedStocks/>
            </div>
        </div>
    );
};

export default Dashboard;
