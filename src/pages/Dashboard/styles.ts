import { makeStyles } from '@mui/styles';

const useDashboardStyles = makeStyles((theme: any) => ({
    container: {
        padding: 0,
        maxWidth: 1310,
        margin: 'auto',
        display: 'flex',
        width: 'calc(100% - 50px)',
        flexDirection: 'column',
        marginTop: 25,
        [theme.breakpoints.up('md')]: {
            flexDirection: 'row',
            padding: 25,
            marginTop: 0,
        },
    },
    search: {
        width: '100%',

        [theme.breakpoints.up('md')]: {
            width: 'calc(50% - 25px)',
        },

        '&:first-child': {
            marginRight: 0,

            [theme.breakpoints.up('md')]: {
                marginRight: 25,
            },
        },

        '&:last-child': {
            marginLeft: 0,
            marginTop: 25,

            [theme.breakpoints.up('md')]: {
                marginLeft: 25,
            },
        }
    },
    divider: {
        height: 'auto !important',
        background: '#E0E0E0',
        width: 1
    }
}));

export default useDashboardStyles;
