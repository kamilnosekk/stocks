import { makeStyles } from '@mui/styles';

const useSavedActionsStyles = makeStyles({
    title: {
        marginBottom: 15,
        fontSize: 14,
        fontWeight: 'bold',
        display: 'block',
    },
    deleteAction: {
        cursor: 'pointer',
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        textAlign: 'center',
        width: '100%',
    },
    companyCell: {
        cursor: 'pointer',
    }
});

export default useSavedActionsStyles;
