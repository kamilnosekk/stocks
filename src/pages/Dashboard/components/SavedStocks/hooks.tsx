import { useCallback, useMemo } from 'react';
import { Stock, StockKeys } from '../../../../utils/models/stock';
import { Actions, usePortfolioContext } from '../../../../contexts/PortfolioContext';
import { generatePath, useNavigate } from 'react-router-dom';
import DeleteOutlineOutlinedIcon from '@mui/icons-material/DeleteOutlineOutlined';
import { routes } from '../../../../utils/consts/route-paths';

export const useStockActions = () => {
    const [ , dispatch ] = usePortfolioContext();

    const deleteStockFromPortfolio = useCallback((row: any) => { // Issue from Lib.
        dispatch(Actions.deleteAction(row));
    }, [ dispatch ]);

    return {
        deleteStockFromPortfolio
    };
};


export const useStockColumns = () => {
    const { deleteStockFromPortfolio } = useStockActions();
    const navigate = useNavigate();

    const columns = useMemo(
        () => [
            {
                label: 'Symbol',
                key: StockKeys.SYMBOL,
            },
            {
                label: 'Name',
                key: StockKeys.NAME,
                handleCellClick: (row: Stock) =>
                    navigate(generatePath(routes.COMPANY_DETAILS, { symbol: row[StockKeys.SYMBOL] })),
            },
        ],
        [navigate]
    );
    const actions = useMemo(
        () => [
            {
                icon: DeleteOutlineOutlinedIcon,
                handler: (row: Stock) => deleteStockFromPortfolio(row)
            }
        ],
        [deleteStockFromPortfolio]
    );
    return { columns, actions };
};
