import React, { useMemo } from 'react';
import useSavedActionsStyles from './styles';
import { usePortfolioContext } from 'contexts/PortfolioContext';
import { useStockColumns } from './hooks';
import Table from 'components/Table';
import { Stock } from '../../../../utils/models/stock';

const SavedStocks: React.FC = () => {
    const classes = useSavedActionsStyles();
    const [ state ] = usePortfolioContext();
    const { columns, actions } = useStockColumns();

    const data = useMemo(() => state.portfolio, [ state ]);

    return (
        <>
            <span className={ classes.title }>Your portfolio</span>
            <Table<Stock> withActions { ...{ columns, data, actions } }/>
        </>
    );
};

export default SavedStocks;
