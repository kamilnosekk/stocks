import { useSendData } from 'hooks/useSendData';
import { SEARCH } from '../../../../utils/consts/api-urls';
import { useCallback } from 'react';
import _ from 'lodash';
import { Stock } from '../../../../utils/models/stock';
import { ApiWrapper } from '../../../../utils/models/api-wrapper';
import { Actions, usePortfolioContext } from '../../../../contexts/PortfolioContext';

export const useSearch = () => {
    const { data, error, loading, makeRequest } = useSendData<unknown, ApiWrapper<Stock[]>>(SEARCH);

    const handleSearch = useCallback(_.debounce((value: string) => searchForActions(value), 500), []);

    const searchForActions = async (value: string) => {
        await makeRequest({
            method: 'get',
            params: [
                { param: 'function', value: 'SYMBOL_SEARCH' },
                { param: 'keywords', value },
            ],
            onError: () => console.log('error'),
        })
    }

    return {
        handleSearch,
        data,
        error,
        loading,
    }
}

export const useSearchPanelActions = () => {
    const [, dispatch] = usePortfolioContext();

    const handleAddStock = useCallback((stock: Stock) => dispatch(Actions.addStock(stock)), [dispatch])

    return {
        handleAddStock
    }
}
