import React from 'react';
import { InputAdornment, Skeleton, TextField } from '@mui/material';
import SearchIcon from '@mui/icons-material/Search';
import StockList from 'pages/Dashboard/components/StockList';
import useSearchPanelStyles from './styles';
import { useSearch, useSearchPanelActions } from './hooks';

const SearchPanel: React.FC = () => {
    const classes = useSearchPanelStyles();
    const { handleAddStock } = useSearchPanelActions();
    const { data, loading, handleSearch } = useSearch();

    return (
        <div>
            <div>
                <TextField
                    label="Company Name"
                    placeholder="Example: Apple"
                    variant="outlined"
                    onChange={({target: { value }}) => handleSearch(value)}
                    InputProps={ {
                        startAdornment: (
                            <InputAdornment position="start">
                                <SearchIcon/>
                            </InputAdornment>
                        ),
                    } }/>
            </div>
            <div className={classes.list}>
                <p className={classes.searchTitle}>Search Results</p>
                { loading
                    ? <>
                        {new Array(4).fill(0).map(() =>
                            <Skeleton className={classes.skeleton} variant="text" height={57} />)}
                      </>
                    : <StockList data={data?.bestMatches || []} { ...{ handleAddStock } }/>}
            </div>
        </div>
    );
};

export default SearchPanel;
