import { makeStyles } from '@mui/styles';

const useSearchPanelStyles = makeStyles({
    list: {
        marginTop: 50,
    },
    searchTitle: {
        fontSize: 14,
        fontWeight: 'bold',
    },
    skeleton: {
        marginTop: '5px !important',
        transform: 'scale(1) !important'
    }
});

export default useSearchPanelStyles;
