import { makeStyles } from '@mui/styles';

const useListStyles = makeStyles({
    wrapper: {
        width: '100%',
        height: '100%',
        border: '1px solid #E0E0E0',
        borderRadius: 5,
    },
    noData: {
        padding: 15,
        display: 'block',
    },
});

export default useListStyles;
