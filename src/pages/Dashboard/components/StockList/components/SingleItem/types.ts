export interface Props {
    symbol: string;
    name: string;

    handleAdd: () => void;
}
