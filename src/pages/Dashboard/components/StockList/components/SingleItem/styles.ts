import { makeStyles } from '@mui/styles';

const useSingleItemStyles = makeStyles({
    item: {
        width: 'calc(100% - 30px)',
        height: '100%',
        padding: 15,
        borderBottom: '1px solid #E0E0E0',
        display: 'flex',
        justifyContent: 'space-between',
        alignItems: 'center',

        '&:last-child': {
            border: 'none',
        }
    },
    title: {
        fontSize: 14,
    },
    add: {
        cursor: 'pointer',
    },
});

export default useSingleItemStyles;
