import React from 'react';
import useSingleItemStyles from './styles';
import AddCircleOutlineOutlinedIcon from '@mui/icons-material/AddCircleOutlineOutlined';
import { Props } from './types';

const SingleItem: React.FC<Props> = ({ name, symbol, handleAdd }) => {
    const classes = useSingleItemStyles();

    return (
        <div className={classes.item}>
            <span className={classes.title}>{ symbol } - { name }</span>
            <span className={classes.add} onClick={handleAdd}>
                <AddCircleOutlineOutlinedIcon/>
            </span>
        </div>
    );
};

export default SingleItem;
