import React from 'react';
import { Props } from './types';
import useListStyles from './styles';
import SingleItem from './components/SingleItem';
import { StockKeys } from 'utils/models/stock';


const StockList: React.FC<Props> = ({ data, handleAddStock }) => {
    const classes = useListStyles();

    return (
        <div className={ classes.wrapper }>
            { data.length > 0
                ? data.map((item) => <SingleItem
                name={ item[StockKeys.NAME] }
                symbol={ item[StockKeys.SYMBOL] }
                key={item[StockKeys.SYMBOL] }
                handleAdd={ () => handleAddStock(item) }/>)
                : <span className={ classes.noData }>No data to display</span>
            }
        </div>
    );
};

export default StockList;
