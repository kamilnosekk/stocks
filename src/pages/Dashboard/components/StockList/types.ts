import { Stock } from '../../../../utils/models/stock';

export interface Props {
    data: Stock[];

    handleAddStock: (stock: Stock) => void;
}
