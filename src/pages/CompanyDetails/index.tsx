import React from 'react';
import { useCompanyDetails } from './hooks';
import { Button } from '@mui/material';
import useCompanyDetailsStyles from './styles';

const CompanyDetails = () => {
    const classes = useCompanyDetailsStyles();
    const { data, navigateToDashboard } = useCompanyDetails();

    return (
        <div className={classes.container}>
            <Button variant='outlined' onClick={navigateToDashboard}>Go Back</Button>
            <div className={classes.name}>
                <h2>{ data?.Name}</h2>
            </div>
            <p className={classes.details}>
                <strong>Address: </strong>
                { data?.Address}
            </p>
            <p className={classes.details}>
                <strong>Market Capitalization:</strong>
                { data?.SharesOutstanding}
            </p>
            <p className={classes.description}>{ data?.Description}</p>
        </div>
    );
};

export default CompanyDetails;
