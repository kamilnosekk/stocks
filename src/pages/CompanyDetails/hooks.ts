import { useSendData } from 'hooks/useSendData';
import { SEARCH } from 'utils/consts/api-urls';
import { useEffect } from 'react';
import { useNavigate, useParams } from 'react-router-dom';
import { routes } from 'utils/consts/route-paths';
import { CompanyDetails } from 'utils/models/company-details';

export const useCompanyDetails = () => {
    const { symbol } = useParams();
    const navigate = useNavigate();
    const { data, loading, error, makeRequest } = useSendData<unknown, CompanyDetails>(SEARCH)


    useEffect(() => {
        const getCompanyDetails = async () => {
            await makeRequest({
                method: 'get',
                params: [
                    { param: 'function', value: 'OVERVIEW' },
                    { param: 'symbol', value: symbol || '' },
                ],
            })
        }

        getCompanyDetails();
    }, [symbol])

    const navigateToDashboard = () => {
        navigate(routes.DASHBOARD)
    }

    return {
        data,
        loading,
        error,
        navigateToDashboard,
    }
}
