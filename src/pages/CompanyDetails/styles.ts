import { makeStyles } from '@mui/styles';

const useCompanyDetailsStyles = makeStyles({
    container: {
        padding: 25,
        maxWidth: 1310,
        margin: 'auto',
        width: 'calc(100% - 50px)',
    },
    name: {
        marginTop: 40
    },
    details: {
        fontSize: 14,
        marginTop: 0,

        '& > strong': {
            marginRight: 10,
        }
    },
    description: {
        marginTop: 25,
        fontSize: 14,
    }
});

export default useCompanyDetailsStyles;
