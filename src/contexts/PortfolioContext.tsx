import React, { createContext, useCallback, useContext, useReducer } from 'react';
import { Stock, StockKeys } from '../utils/models/stock';
import { useLocalStorage } from '../hooks/useLocalStorage';

export interface PortfolioContext {
    portfolio: Stock[];
}

const portfolio: PortfolioContext = {
    portfolio: [],
};

type Action<T> = { type: string; payload: T }

const reducer = (state: PortfolioContext, action: Action<Stock>) => {
    switch (action.type) {
        case 'ADD_STOCK':
            const isIncluded = state.portfolio.some(item => item[StockKeys.SYMBOL] === action.payload[StockKeys.SYMBOL]);

            return isIncluded
                ? { ...state }
                : { ...state, portfolio: [ ...state.portfolio, {...action.payload, id: action.payload[StockKeys.SYMBOL] }] };
        case 'REMOVE_PORTFOLIO':
            console.log(action.payload, state.portfolio);
            return {
                ...state,
                portfolio: state.portfolio.filter(item => item[StockKeys.SYMBOL] !== action.payload[StockKeys.SYMBOL])
            };
        default:
            return { ...state };
    }
};

export const PortfolioContext = createContext(portfolio) as any;

export const PortfolioContextProvider: React.FC = ({ children }) => {
    const { getFromLocalStorage } = useLocalStorage();
    const getPortfolios = useCallback(() => getFromLocalStorage('portfolios') || { portfolio: [] }, [ getFromLocalStorage ]);
    const [ state, dispatch ] = useReducer(reducer, portfolio, () => getPortfolios());

    return (
        <PortfolioContext.Provider value={ [ state, dispatch ] }>
            { children }
        </PortfolioContext.Provider>
    );
};

export const usePortfolioContext = () => useContext<any>(PortfolioContext);


export const Actions = {
    deleteAction: (stock: Stock) => ({
        type: 'REMOVE_PORTFOLIO',
        payload: stock,
    }),
    addStock: (stock: Stock) => ({
        type: 'ADD_STOCK',
        payload: stock
    })
}
